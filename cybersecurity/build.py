import finn.builder.build_dataflow as build
import finn.builder.build_dataflow_config as build_cfg

import os
import shutil
import time

model_file = "/home/antoine/Documents/coyote_finn/cybersecurity/cybsec-mlp-ready.onnx"
output_dir = "/home/antoine/Documents/coyote_finn/cybersecurity/outputs_u250_automate"

"""
class BuildType(IntEnum):
    STITCHED_IP = (9,)
    BITFILE = (2,)
    XO_FILES = (4,)
    FROM_STITCHED_IP = 8


build_type = BuildType.XO_FILES

start_step = None
if build_type & BuildType.FROM_STITCHED_IP:
    start_step = "step_measure_rtlsim_performance"
else:
    start_step = None
    delete_previous()


if build_type & BuildType.BITFILE:
    generate_outputs = [
        build_cfg.DataflowOutputType.BITFILE,
    ]
elif build_type & BuildType.STITCHED_IP:
    generate_outputs = [
        build_cfg.DataflowOutputType.STITCHED_IP,
    ]
elif build_type & BuildType.XO_FILES:
    generate_outputs = [
        build_cfg.DataflowOutputType.XO_FILES,
    ]
print(f"Start step is {start_step}")
"""


def delete_previous():
    # Delete previous run results if exist
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
        print("Previous run results deleted!")


# delete_previous()
cfg_build = build.DataflowBuildConfig(
    output_dir=output_dir,
    mvau_wwidth_max=80,
    target_fps=1000,
    synth_clk_period_ns=10.0,
    board="U250",
    shell_flow_type=build_cfg.ShellFlowType.COYOTE_ALVEO,
    steps=None,
    start_step=None,
    save_intermediate_models=True,
    generate_outputs=[build_cfg.DataflowOutputType.BITFILE],
    folding_config_file="/home/antoine/Documents/coyote_finn/cybersecurity/outputs_u250_automate/custom_config.json",
    verbose=False,
)

step_start = time.time()
build.build_dataflow_cfg(model_file, cfg_build)
step_end = time.time()
print(f"Build took {step_end - step_start}")
